<?php

namespace Dotcubed\Tests;

use Dotcubed\CompanyHouseApi\CompanyHouse;

class CompanyHouseTest extends TestAbstract
{

    public function testSearch()
    {
        $number = 11333911; // Valid
        $object = CompanyHouse::config('UzhZY2dFVXo2MVFOcXV1Rk16TXBwOEFuR1ozam1ibFdrYVRMT2NLNzo=');
        $response = $object->search($number);

        $this->assertEquals(200, $response['status']);
        $this->assertNotEmpty($response['data']);
    }

    public function testExceptionGetCompanyByNumber()
    {
        $this->expectException(\Exception::class);

        $number = 113339112; // Invalid
        $object = CompanyHouse::config('UzhZY2dFVXo2MVFOcXV1Rk16TXBwOEFuR1ozam1ibFdrYVRMT2NLNzo=');

        $object->search($number);
    }
}