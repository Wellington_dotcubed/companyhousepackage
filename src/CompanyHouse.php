<?php


namespace Dotcubed\CompanyHouseApi;


class CompanyHouse implements CompanyHouseInterface
{
    protected $api_key;
    protected $api = 'https://api.companieshouse.gov.uk/company';

    public function __construct($token)
    {
        $this->api_key = $token;
    }

    /**
     * @param $token
     * @return CompanyHouse|mixed
     */
    static function config($token)
    {
        return new CompanyHouse($token);
    }

    /**
     * Search Company by number
     * @param $number
     * @return mixed
     * @throws \Exception
     */
    public function search($number)
    {
        $response = $this->request($this->api . '/' . $number);
        return $response;
    }

    /**
     * REQUEST
     * @param string $uri URI object
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function request($uri, $data = array())
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $uri,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic " . $this->api_key,
                "cache-control: no-cache"
            ),
        ]);

        $resp = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($code == 404) {
            header("HTTP/1.0 404 Not Found");
            return json_encode(['message' => 'No data found!', 'status' => 404]);
        }
        return json_encode(['message' => $resp, 'status' => 200]);
    }
}