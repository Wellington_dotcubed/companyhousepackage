<?php


namespace Dotcubed\CompanyHouseApi;


interface CompanyHouseInterface
{
    const VERSION = '1.0.0';

    /**
     *  REQUEST
     * @param string $uri URI object
     * @param array $data
     * @return mixed
     */
    function request($uri, $data = array());

    /**
     * CONFIG
     * @param $token
     * @return mixed
     */
    public static function config($token);
}